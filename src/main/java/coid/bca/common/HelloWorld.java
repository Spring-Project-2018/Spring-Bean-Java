package coid.bca.common;

import java.util.Date;

public class HelloWorld {

	private String name;
	private Date createdDate;
	
	public HelloWorld() {
		createdDate = new Date();
	};
	
	public HelloWorld(String name) {
		this.name = name;
		createdDate = new Date();
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void printHello() {
		if (name == null || name.isEmpty()) {
			System.out.println("Hello World!" + ", created : " + createdDate);
		}
		else {
			System.out.println("Hello !" + name + ", created : " + createdDate);
		}
	}
}
