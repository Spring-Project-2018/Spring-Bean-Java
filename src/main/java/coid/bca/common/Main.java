package coid.bca.common;

import java.util.concurrent.TimeUnit;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

	public static void main(String[] args) {

		@SuppressWarnings("resource")
		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		
		try {
			HelloWorld hello1 = (HelloWorld) context.getBean("helloWorld");

			TimeUnit.SECONDS.sleep(5);
			
			HelloWorld hello2 = (HelloWorld) context.getBean("helloWorld");
			
			hello1.printHello();
			hello2.printHello();
		} catch (InterruptedException ie) { }
		
		
		
	}

}
