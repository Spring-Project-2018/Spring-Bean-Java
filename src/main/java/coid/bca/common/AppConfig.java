package coid.bca.common;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

public class AppConfig {

	@Bean(name="helloWorld")
	@Scope("prototype")
	public HelloWorld helloWorld() {
		return new HelloWorld("Hello BIT3");
	}
	
}
